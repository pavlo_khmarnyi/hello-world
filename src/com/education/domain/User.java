package com.education.domain;

public class User {
    private String name;
    private int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void printInfo() {
        System.out.println("Hello! I am " + name);
        System.out.println("I am " + age + " years old.\n");
    }
}
