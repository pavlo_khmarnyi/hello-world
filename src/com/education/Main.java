package com.education;

import com.education.domain.User;

public class Main {

    public static void main(String[] args) {
        User john = new User("John", 25);
        User sara = new User("Sara", 23);
        john.printInfo();
        sara.printInfo();
    }
}
